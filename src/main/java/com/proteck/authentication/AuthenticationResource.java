package com.proteck.authentication;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.proteck.authentication.domain.AuthenticationUser;
import com.proteck.authentication.domain.Employee;
import com.proteck.authentication.domain.PermissionDTO;
import com.proteck.authentication.domain.User;
import com.proteck.authentication.requests.AuthenicationUserRequest;
import com.proteck.authentication.requests.AuthenticationActivateEmployee;
import com.proteck.authentication.requests.AuthenticationChangePasswordRequest;
import com.proteck.authentication.requests.AuthenticationChangePasswordResponse;
import com.proteck.authentication.requests.AuthenticationEmployeeRequest;
import com.proteck.authentication.requests.AuthenticationEmployeeResponse;
import com.proteck.authentication.requests.AuthenticationRequest;
import com.proteck.authentication.requests.AuthenticationResponse;
import com.proteck.authentication.requests.AuthenticationUpdateEmployeeRequest;
import com.proteck.authentication.requests.AuthenticationUsernamePasswordRequest;
import com.proteck.authentication.requests.AuthenticationUsernamePasswordResponse;
import com.proteck.authentication.service.AuthenticationService;
import com.proteck.authentication.utils.AuthenticationUtils;
import com.proteck.authentication.utils.StringUtils;

/**
 * Handles requests for the application home page.
 */
@Controller
public class AuthenticationResource {
	
	private static final Logger logger = LoggerFactory.getLogger(AuthenticationResource.class.getName());
	
	private @Value ("${maxLoginAttempts}") String maxLoginAttempts;
	
	private @Value ("${changePasswordUrl}") String changePasswordUrl;
	
	private @Value("${sharedSecret}") String shared;
	/**
	 * 
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private AuthenticationService authenticationServiceImpl;

	@RequestMapping(value = "/authentication/authenticateUser", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public String home(Model model,@RequestBody String request) {
		String result = null;
		AuthenticationResponse response = null;
		String authenticationRequestString = new String(Base64.decodeBase64(request));
		AuthenticationRequest authenticationRequest = null;
		try{
			authenticationRequest = AuthenticationUtils.convertFromJSON(AuthenticationRequest.class, authenticationRequestString);
		}catch(IOException e){
			response = new AuthenticationResponse(false);
			response.setMessage("Authentication Request Object is null or cannot be processed.");
			
		}
		String username = authenticationRequest.getUsername().trim();
		String password = authenticationRequest.getPassword();
		boolean authenticationResult = false;
		ObjectMapper mapper = new ObjectMapper();
		
		String[] permissions = null;
		
		logger.info("User: "+username+" is being authenticated right..Welcome to Authentication Service.");
		if(StringUtils.isNotNullorBlank(username) && StringUtils.isNotNullorBlank(password)){
			AuthenticationUser authenticationUser = null;
			try {
				
				authenticationUser = authenticationServiceImpl.findUserByUsername(username);				
				if(authenticationUser != null){
					if(authenticationUser.getUser().isAccountLocked()){
						response = new AuthenticationResponse();
						result = "User with username: "+username+" account is locked.";
						logger.info(result);
						authenticationResult = false;
						response.setMessage(result);
						response.setUserId(0);						
					}else if(authenticationUser.getUser().isAccountExpired()){
						response = new AuthenticationResponse();
						result = "User with username: "+username+" account is expired.";
						logger.info(result);
						authenticationResult = false;
						response.setMessage(result);
						response.setUserId(0);	
					}else{
						response = authenticationServiceImpl.authenticateUser(authenticationUser.getUser(),password);
						if(response.isResult()){
							response.setMessage("Authentication valid");
							AuthenticationEmployeeResponse employee = authenticationServiceImpl.findEmployeeByUsername(username);
							response.setEmployee(employee.getEmployee());
							response.setUserId(authenticationUser.getUser().getUserId());
							
							permissions = AuthenticationUtils.convertListToArray(authenticationUser.getPermissionList());
							response.setPermissions(permissions);
						}
						authenticationResult = response.isResult();
					}
				
				}else{
					response = new AuthenticationResponse();
					result = "User with username: "+username+" is invalid.";
					logger.info(result);
					authenticationResult = false;
					response.setMessage(result);
					response.setUserId(0);
				}
				if(response.getUserId() > 0){
					updateEmployeeLoginAttempt(response);
					
				}
				response.setResult(authenticationResult);
				result = mapper.writeValueAsString(response);
			} catch (JsonProcessingException e) {
				logger.warn(e.getMessage(),e);	
			}finally{
				if(response.getUserId() > 0){
					authenticationServiceImpl.saveLoginTime(authenticationUser.getUser().getUsername(),authenticationUser.getUser().getCompanyId(),response.isResult());
				}
				permissions = null;
				mapper = null;
			}
		}

		return result;
	}

	
	private int updateEmployeeLoginAttempt(AuthenticationResponse response){
		int loginAttempt = response.getLoginAttempts();
		loginAttempt++;
		response.setLoginAttempts(loginAttempt);
		return loginAttempt;
	}
	@RequestMapping(value = "/authentication/getUserPermissions", method = RequestMethod.GET)
	@ResponseBody
	public String[] getEmployeePermission(@RequestBody int userId){
		List<String> permissionList = null;
		String[] permissions = null;
		
		if(userId > 0){
			permissionList = authenticationServiceImpl.findEmployeePermissionByUserId(userId);
			permissions = AuthenticationUtils.convertListToArray(permissionList);
		}
		
		return permissions;
	}
	
	@RequestMapping(value = "/authentication/getAllEmployeePermission", method = RequestMethod.GET)
	
	public @ResponseBody List<PermissionDTO> getAllEmployeePermissionByEmployeeNumber(){
		List<PermissionDTO> permissionList = null;
		logger.info("Retrieving All Permissions for Employee");
			permissionList = authenticationServiceImpl.getAllEmployeePermission();
		logger.info("Permissions retrieved are: "+permissionList);
		
		return permissionList;
	}
	@RequestMapping(value="/authentication/findEmployeeByUsername", method=RequestMethod.GET)
	public @ResponseBody AuthenticationEmployeeResponse findEmployeeByUsername(@RequestParam("username")  String username){
		AuthenticationEmployeeResponse employeeResponse = null;
		if(username != null && username.length() > 3){
			employeeResponse = authenticationServiceImpl.findEmployeeByUsername(username);
		}else{
			employeeResponse = new AuthenticationEmployeeResponse();
			employeeResponse.setEmployee(null);
			employeeResponse.setResult(false);
			employeeResponse.setMessage("Username: "+username+" is invalid or contains invalid characters.");
		}
		return employeeResponse;
	}
	@RequestMapping(value="/authentication/findEmployeeByEmployeeId", method=RequestMethod.GET)
	public @ResponseBody AuthenticationEmployeeResponse findEmployeeByEmployeeId(@RequestParam ("employeeId") String employeeId, @RequestParam("companyId") String companyId){
		AuthenticationEmployeeResponse employeeResponse = null;
		try{
			
			if(employeeId != null && !companyId.isEmpty()){
				Integer empId = Integer.parseInt(employeeId);
				Integer compId = Integer.parseInt(companyId);
				employeeResponse = authenticationServiceImpl.findEmployeeByEmployeeId(empId,compId);
			}else{
				employeeResponse = new AuthenticationEmployeeResponse();
				employeeResponse.setEmployee(null);
				employeeResponse.setResult(false);
				employeeResponse.setMessage("Employee Id: "+employeeId+" is invalid or contains invalid characters.");
			}
		}catch(Exception e){
			logger.warn(e.getMessage(),e);
			employeeResponse = new AuthenticationEmployeeResponse();
			employeeResponse.setEmployee(null);
			employeeResponse.setResult(false);
			employeeResponse.setMessage("Error occured");
		}
			return employeeResponse;		
	}
	@RequestMapping(value="/authentication/getAllEmployeeByCompanyId", method=RequestMethod.GET)
	public @ResponseBody  List<Employee> getEmployeeByCompanyId(@RequestParam("companyId") int companyId){
		List<Employee> employeeList = null;
		if(companyId > 0){
			employeeList = authenticationServiceImpl.getAllEmployeeByCompanyId(companyId);
		}
		return employeeList;
	}
	
	@RequestMapping(value="/authentication/doEmployeeEmailExist", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody boolean doEmployeeEmailExist(@RequestParam("emailAddress") String emailAddress){
		boolean emailExist = false;
		if(emailAddress != null && emailAddress.length() > 3){
			emailExist = authenticationServiceImpl.doEmployeeEmailExist(emailAddress);
		}
		return emailExist;
	}
	///authentication/isPhoneUnique
	@RequestMapping(value="/authentication/isPhoneUnique", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody boolean isPhoneUnique(@RequestParam("phoneNumber") String phoneNumber){
		boolean phoneNumberExist = false;
		
		if(phoneNumber != null && phoneNumber.length() > 3){
			phoneNumberExist = authenticationServiceImpl.doPhoneNumberExist(phoneNumber.replaceAll("-", ""));
		}
		return phoneNumberExist;
	}	
	@RequestMapping(value="/authentication/saveEmployeeCredentials", method=RequestMethod.POST,consumes="application/json")
	public @ResponseBody AuthenticationResponse saveEmployeeCredentials(@RequestBody String employeeRequest){
		AuthenticationResponse response = null;
		AuthenticationEmployeeRequest request = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			request = mapper.readValue(employeeRequest, AuthenticationEmployeeRequest.class);

			response = authenticationServiceImpl.saveEmployeeInformation(request);
			if(request.getRequestType().equalsIgnoreCase("company")){
				if(response.isResult()){
					response = authenticationServiceImpl.saveLoginCredentials(request,response);
					if(response.isResult()){
						response = authenticationServiceImpl.addUserPermissions(request.getRoleId(),request.getCompanyId(),response.getUserId());
					}
				}
			}
		} catch (IOException e) {
			logger.warn(e.getMessage(),e);	
		}finally{
			mapper = null;
		}
		return response;
	}

	@RequestMapping(value="/authentication/saveUserCredentials", method=RequestMethod.POST,consumes="application/json")
	public @ResponseBody AuthenticationResponse saveUserCredentials(@RequestBody String userRequest){
		AuthenticationResponse response = null;
		AuthenicationUserRequest request = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			request = mapper.readValue(userRequest, AuthenicationUserRequest.class);
			String hashPassword = generateHashPassword(request);
			if(request.getRequestType().equalsIgnoreCase("company")){
				if(response.isResult()){
					response = authenticationServiceImpl.saveLoginInformation(request);
					if(response.isResult()){
						response = authenticationServiceImpl.addUserPermissions(request.getRoleId(),request.getCompanyId(),response.getUserId());
					}
				}
			}
		} catch (IOException e) {
			logger.warn(e.getMessage(),e);	
		}finally{
			mapper = null;
		}
		return response;
	}
	
	private String generateHashPassword(AuthenicationUserRequest request) {
		String raw = request.getPassword();
		String sharedKey = request.getOwass();
		StringBuilder sbr = new StringBuilder();
		String passwd = sbr.append(sharedKey).append(raw).append(shared).toString();
		return BCrypt.hashpw(passwd, BCrypt.gensalt());
	}


	@RequestMapping(value="/authentication/isUserAuthorized", method=RequestMethod.GET, consumes="application/json", produces="application/json")
	public @ResponseBody boolean  isUserAuthorized(@RequestBody AuthenticationRequest authenticationRequest){
		boolean isAuthorised = true;
		return isAuthorised;
	}
	
	@RequestMapping(value="/authentication/changePassword", method=RequestMethod.POST, consumes="application/json", produces="application/json")
	public @ResponseBody Boolean forgotPassword(@RequestBody AuthenticationUsernamePasswordRequest authenticationRequest,HttpServletRequest request){
		
		AuthenticationUsernamePasswordResponse response = null;
		
		String emailAddress = authenticationRequest.getEmailAddress();
		String username = authenticationRequest.getUsername();
		String lastname = authenticationRequest.getLastname();
		String url = System.getenv("api").concat(changePasswordUrl);
		if(StringUtils.isNotNullorBlank(username) && StringUtils.isNotNullorBlank(emailAddress) && StringUtils.isNotNullorBlank(lastname)){
			response = authenticationServiceImpl.forgotPasswordService(emailAddress,username,lastname,url);
		}else{
			response = new AuthenticationUsernamePasswordResponse();
			response.setResult(false);
		}
		return response.isResult();
	}
	
	@RequestMapping(value="/authentication/forgotUsername", method=RequestMethod.POST)
	public @ResponseBody  AuthenticationUsernamePasswordResponse forgotUsername(@RequestBody AuthenticationUsernamePasswordRequest authenticationRequest){
		logger.info("Authentication Service is searching for username for last name: "+authenticationRequest.getLastname()+" and Email address: "+authenticationRequest.getEmailAddress());
		AuthenticationUsernamePasswordResponse response = null;
		String emailAddress = authenticationRequest.getEmailAddress();
		String lastname = authenticationRequest.getLastname();		
		try{
			if(StringUtils.isNotNullorBlank(emailAddress) && StringUtils.isNotNullorBlank(lastname)){
				response = authenticationServiceImpl.findUserByUsername(emailAddress,lastname);
				if(response != null && !response.getUsername().isEmpty()){
					response.setResult(true);
				}else{
					response = new AuthenticationUsernamePasswordResponse();
					response.setResult(false);
				}
			}else{
				response = new AuthenticationUsernamePasswordResponse();
				response.setResult(false);
		}
			logger.info("Authentication Service searched for username for last name: "+authenticationRequest.getLastname()+" and Email address: "+authenticationRequest.getEmailAddress()+" returned: "+response.isResult());
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
			response = new AuthenticationUsernamePasswordResponse();
			response.setResult(false);
		}finally{
			emailAddress = null;
			lastname = null;
		}
		
		
		return response;
	}
	
	@RequestMapping(value="/employee/updateEmployeeInfo", method=RequestMethod.POST)
	public @ResponseBody AuthenticationEmployeeResponse updateEmployeeInfo(@RequestBody AuthenticationUpdateEmployeeRequest request) {
		AuthenticationEmployeeResponse employeeResponse =  null;
		if(request != null){
			employeeResponse = authenticationServiceImpl.updateEmployeeInformation(request);
		}

		return employeeResponse;
		
	}

	@RequestMapping(value="/authentication/validateEmployeeToken", method=RequestMethod.POST)
	public @ResponseBody boolean validateEmployeeToken(@RequestBody String token){
		boolean isValid = false;
		isValid = authenticationServiceImpl.validateEmployeeToken(token,LocalDateTime.now());
		return isValid;
	}
	
	@RequestMapping(value="/authentication/validatePasswordToken", method=RequestMethod.POST)
	public @ResponseBody boolean validatePasswordToken(@RequestBody String token){
		boolean isValid = false;
		isValid = authenticationServiceImpl.validatePasswordToken(token,LocalDateTime.now());
		return isValid;
	}
	
	@RequestMapping(value="/authentication/processChangePassword", method=RequestMethod.POST)
	public @ResponseBody AuthenticationChangePasswordResponse processChangePassword(@RequestBody AuthenticationChangePasswordRequest request){
		AuthenticationChangePasswordResponse response = null;
		if(request.getRequestType().equalsIgnoreCase("forgotPassword")){
			response = authenticationServiceImpl.forgotPassword(request);
		}else{
			response = authenticationServiceImpl.changePassword(request);
		}
		
		return response;
	}

	@RequestMapping(value="/authentication/processActivateEmployee", method=RequestMethod.POST)
	public @ResponseBody AuthenticationChangePasswordResponse processActivateEmployee(@RequestBody AuthenticationActivateEmployee request){
		AuthenticationChangePasswordResponse response = null;

			response = authenticationServiceImpl.activateEmployee(request);

		
		return response;
	}
	
	@RequestMapping(value="/authentication/test", method=RequestMethod.GET,produces="application/json")
	public @ResponseBody String test(){
		String response = "working";

		
		return response;
	}
	
	@RequestMapping(value="/authentication/removeCompanyEmployee", method=RequestMethod.POST,produces="application/json")
	public @ResponseBody AuthenticationResponse deleteEmployeeAndUsers(@RequestBody AuthenticationRequest authenticationRequest){
		AuthenticationResponse response = null;
		if(authenticationRequest != null){
			Integer companyId = authenticationRequest.getCompanyId();
			if(companyId > 0){
				response = authenticationServiceImpl.deleteCompanyEmployeesAndUsers(companyId);
			}else{
				response = new AuthenticationResponse();
				response.setResult(false);
				response.setMessage("Company Id is null or is invalid.");
			}
		}else{
			response = new AuthenticationResponse();
			response.setResult(false);
			response.setMessage("Company Id is null or is invalid.");
		}
		return response;
	}
}
