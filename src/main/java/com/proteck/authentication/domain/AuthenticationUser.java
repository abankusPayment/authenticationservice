package com.proteck.authentication.domain;

import java.util.List;

public final class AuthenticationUser {
	private final User user;
	
	private final List<String> permissionList;

	private Employee employee;
	
	public AuthenticationUser(User user, List<String> permissionList) {
		super();
		this.user = user;
		this.permissionList = permissionList;
	}

	public User getUser() {
		return user;
	}

	public List<String> getPermissionList() {
		return permissionList;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
}
