package com.proteck.authentication.domain;

import java.util.UUID;

public class EmployeeLogin {

	private UUID employeeLoginId;
	
	private long employeeId;
	
	private int companyId;
	
	private String loginDate;
	
	private String loginState;
	
	private String ipAddress;

	public UUID getEmployeeLoginId() {
		return employeeLoginId;
	}

	public void setEmployeeLoginId(UUID employeeLoginId) {
		this.employeeLoginId = employeeLoginId;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(String loginDate) {
		this.loginDate = loginDate;
	}

	public String getLoginState() {
		return loginState;
	}

	public void setLoginState(String loginState) {
		this.loginState = loginState;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	
}
