package com.proteck.authentication.domain;

public class TempEmployeeAccount {
	private int tempId;
	
	private int companyId;
	
	private String emailAddress;
	
	private String token;
	
	private long dateCreated;
	
	private String tempPassword;
	
	private int employeeId;
	
	private int[] roleId;

	public int getTempId() {
		return tempId;
	}

	public void setTempId(int tempId) {
		this.tempId = tempId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public long getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(long dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getTempPassword() {
		return tempPassword;
	}

	public void setTempPassword(String tempPassword) {
		this.tempPassword = tempPassword;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public int[] getRoleId() {
		return roleId;
	}

	public void setRoleId(int[] roleId) {
		
		this.roleId = roleId;
	}
	
	public void setRoleId(int roleId){
		int[] role = new int[1];
		role[0] = roleId;
		this.roleId = role;
	}
	
	
}
