package com.proteck.authentication.requests;

public class AuthenicationUserRequest {
	private String username;
	
	private String password;
	
	private String employeeRole;
	
	private String requestType;
	
	private int companyId;
	
	private int roleId;
	

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	/**
	 * Shared  Secret
	 */
	private String owass;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmployeeRole() {
		return employeeRole;
	}

	public void setEmployeeRole(String employeeRole) {
		this.employeeRole = employeeRole;
	}


	public String getOwass() {
		return owass;
	}

	public void setOwass(String owass) {
		this.owass = owass;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	
	
}
