package com.proteck.authentication.requests;

import com.proteck.authentication.domain.Employee;

public class AuthenticationResponse {


	private boolean result;

	private String message;

	private int userId;
	
	private long employeeId;
	
	private String[] permissions;
	
	private int loginAttempts;
	
	private Employee employee;
	
	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	/**
	 * @param result
	 */
	public AuthenticationResponse(boolean result) {
		this.result = result;
	}
	
	public AuthenticationResponse() {
		//Default Constructor
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String[] getPermissions() {
		return permissions;
	}

	public void setPermissions(String[] permissions) {
		this.permissions = permissions;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public int getLoginAttempts() {
		return loginAttempts;
	}

	public void setLoginAttempts(int loginAttempts) {
		this.loginAttempts = loginAttempts;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
		
	}
	public Employee  getEmployee() {
		return this.employee;
		
	}
	
}
