package com.proteck.authentication.requests;

public class EmployeeRegistrationRequest {

	private int companyId;
	
	private long employeeId;
	
	private String emailAddress;
	
	private String name;
	
	private String tempPassword;
	
	private String url;
	
	private String verificationCode;

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getName() {
		return name;
	}

	public void setName(String firstname) {
		this.name = firstname;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		if(verificationCode != null){
			url = url+verificationCode;
		}
		this.url = url;
	}

	public String getVerificationCode() {

		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {

		this.verificationCode = verificationCode;
	}

	public String getTempPassword() {
		return tempPassword;
	}

	public void setTempPassword(String tempPassword) {
		this.tempPassword = tempPassword;
	}
	
	
}
