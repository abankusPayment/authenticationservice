package com.proteck.authentication.requests;

public class AuthenticationUsernamePasswordResponse {
	private int companyId;
	
	private long employeeId;
	
	private String emailAddress;

	private boolean result;
	
	private String username;
	
	/**
	 * Full name or first or last name
	 */
	private String name;
	
	private String verificationCode;
	
	private String url;

	private String tempPassword;
	
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		StringBuilder sb = new StringBuilder();
		sb.append(verificationCode);
		this.verificationCode = sb.toString();
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		if(verificationCode != null){
			url = url+verificationCode;
		}
		this.url = url;
	}

	
	public String getTempPassword() {
		return tempPassword;
	}

	public void setTempPassword(String generateTempPassword) {
		this.tempPassword = generateTempPassword;
		
	}
	
	
}
