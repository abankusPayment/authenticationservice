package com.proteck.authentication.service;

import java.sql.Connection;
import java.time.LocalDateTime;
import java.util.List;

import com.proteck.authentication.domain.AuthenticationUser;
import com.proteck.authentication.domain.Employee;
import com.proteck.authentication.domain.PermissionDTO;
import com.proteck.authentication.domain.User;
import com.proteck.authentication.requests.AuthenicationUserRequest;
import com.proteck.authentication.requests.AuthenticationActivateEmployee;
import com.proteck.authentication.requests.AuthenticationChangePasswordRequest;
import com.proteck.authentication.requests.AuthenticationChangePasswordResponse;
import com.proteck.authentication.requests.AuthenticationEmployeeRequest;
import com.proteck.authentication.requests.AuthenticationEmployeeResponse;
import com.proteck.authentication.requests.AuthenticationResponse;
import com.proteck.authentication.requests.AuthenticationUpdateEmployeeRequest;
import com.proteck.authentication.requests.AuthenticationUsernamePasswordResponse;

public interface AuthenticationService {
	AuthenticationUser findUserByUsername(String username);
	
	AuthenticationResponse authenticateUser(User user, String password);
	
	AuthenticationEmployeeResponse findEmployeeByUserId(int employeeId);
	
	List<Employee> getAllEmployeeByCompanyId(int companyId);
	
	AuthenticationEmployeeResponse findEmployeePermissionsByUsername(String username);
	
	AuthenticationEmployeeResponse findEmployeeByUsername(String username);
	
	List<String> findEmployeePermissionByUserId(Integer userId);
	

	void saveLoginTime(String username, int companyId, Boolean result);

	boolean doEmployeeEmailExist(String emailAddress);

	List<PermissionDTO> getAllEmployeePermission();

	AuthenticationResponse saveEmployeeInformation(AuthenticationEmployeeRequest employeeRequest);

	AuthenticationEmployeeResponse updateEmployeeInformation(AuthenticationUpdateEmployeeRequest employeeRequest);
	
	AuthenticationResponse saveLoginCredentials(AuthenticationEmployeeRequest request,AuthenticationResponse response);

	boolean addCompanySecurity(int companyId, Connection conn);

	AuthenticationUsernamePasswordResponse findUserByUsername(String emailAddress, String username, String lastname);

	AuthenticationUsernamePasswordResponse findUserByUsername(String emailAddress, String lastname);

	void sendEmail(AuthenticationUsernamePasswordResponse response);

	AuthenticationEmployeeResponse findEmployeeByEmployeeId(long employeeId);

	AuthenticationUsernamePasswordResponse forgotPasswordService(String emailAddress, String username, String lastname, String changePasswordUrl);

	boolean validatePasswordToken(String token, LocalDateTime localDateTime);

	AuthenticationChangePasswordResponse changePassword(AuthenticationChangePasswordRequest request);

	AuthenticationChangePasswordResponse forgotPassword(AuthenticationChangePasswordRequest request);

	boolean doPhoneNumberExist(String phoneNumber);

	boolean validateEmployeeToken(String token, LocalDateTime now);

	AuthenticationChangePasswordResponse activateEmployee(AuthenticationActivateEmployee request);

	/**
	 * This is an overloaded method with the implementing method having a connection parameter
	 * while this method does not have a Connection paramater.
	 * 
	 * @param roleId
	 * @param companyId
	 * @param userId
	 * @return
	 */
	AuthenticationResponse addUserPermissions(int[] roleId, long companyId, int userId);
	
	AuthenticationResponse addUserPermissions(int roleId, long companyId, int userId);

	AuthenticationResponse addUserPermissions(int[] roleId, long companyId, int userId, Connection conn);

	AuthenticationEmployeeResponse findEmployeeByEmployeeId(int employeeId, int companyId);

	AuthenticationResponse deleteCompanyEmployeesAndUsers(Integer companyId);

	AuthenticationResponse saveLoginInformation(AuthenicationUserRequest request);
}
