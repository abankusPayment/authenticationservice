package com.proteck.authentication.service.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import javax.sql.DataSource;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mysql.jdbc.Statement;
import com.proteck.authentication.database.configuration.CassandraConfig;
import com.proteck.authentication.database.configuration.DBConfiguration;
import com.proteck.authentication.domain.AuthenticationUser;
import com.proteck.authentication.domain.Employee;
import com.proteck.authentication.domain.EmployeeLogin;
import com.proteck.authentication.domain.PermissionDTO;
import com.proteck.authentication.domain.TempEmployeeAccount;
import com.proteck.authentication.domain.TempUserAccount;
import com.proteck.authentication.domain.User;
import com.proteck.authentication.exception.PlatformException;
import com.proteck.authentication.requests.AuthenicationUserRequest;
import com.proteck.authentication.requests.AuthenticationActivateEmployee;
import com.proteck.authentication.requests.AuthenticationChangePasswordRequest;
import com.proteck.authentication.requests.AuthenticationChangePasswordResponse;
import com.proteck.authentication.requests.AuthenticationEmployeeRequest;
import com.proteck.authentication.requests.AuthenticationEmployeeResponse;
import com.proteck.authentication.requests.AuthenticationResponse;
import com.proteck.authentication.requests.AuthenticationUpdateEmployeeRequest;
import com.proteck.authentication.requests.AuthenticationUsernamePasswordResponse;
import com.proteck.authentication.requests.EmployeeRegistrationRequest;
import com.proteck.authentication.requests.MessageRequest;
import com.proteck.authentication.service.AuthenticationService;
import com.proteck.authentication.utils.AuthenticationUtils;
import com.proteck.authentication.utils.StringUtils;

public class AuthenticationServiceImpl implements AuthenticationService{

	private static final Logger logger = LoggerFactory.getLogger(AuthenticationServiceImpl.class.getName());

	private String activateEmployeeUrl= System.getenv("activateUrl");
	
	private String hostname = System.getenv("apiUrl");
	
	@Autowired
	private DBConfiguration dbConfiguration;
	
	@Autowired
	private CassandraConfig cassandraDB;
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private AmqpTemplate changePasswordTemplate;
	
	@Autowired
	private AmqpTemplate employeeRegistrationTemplate;
	
	@Override
	public AuthenticationUser findUserByUsername(String username) {
		AuthenticationUser authenticationUser = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("select * from user where username =?");
			ps.setString(1,username);
			rs = ps.executeQuery();
			if(rs.next()){
				User user = new User();
				user.setCompanyId(rs.getInt("companyId"));
				user.setPassword(rs.getString("password"));
				user.setEnabled(rs.getBoolean("enabled"));
				user.setUsername(rs.getString("username"));
				user.setUserId(rs.getInt("userId"));
				user.setAccountExpired(rs.getBoolean("accountExpired"));
				user.setAccountLocked(rs.getBoolean("accountLocked"));
				List<String> userPermissions = findEmployeePermissionByUserId(conn,user.getUserId(),user.getCompanyId());
				authenticationUser = new AuthenticationUser(user,userPermissions);

			}
		} catch (SQLException e) {
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(null,ps,rs);
		}
		return authenticationUser;
	}


	private List<String> findEmployeePermissionByUserId(Connection conn, int userId, int companyId) {
		List<String> permissionList = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement("select p.* from permissions p JOIN userpermission up ON up.permissionId = p.permissionId where up.userId =?");
			ps.setInt(1,userId);
			rs = ps.executeQuery();
			permissionList = new ArrayList<String>();
			while(rs.next()){
				permissionList.add(rs.getString("permission"));
				
			}
		} catch (SQLException e) {
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(null,ps,rs);
		}
		return permissionList;
	}

	@Override
	public List<String> findEmployeePermissionByUserId(Integer userId) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<String> permissionList = null;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("select p.* from permissions p JOIN userpermission up ON up.permissionId = p.permissionId where up.userId =?");
			ps.setInt(1,userId);
			rs = ps.executeQuery();
			permissionList = new ArrayList<String>();
			while(rs.next()){
				permissionList.add(rs.getString("permission"));
				
			}
		} catch (SQLException e) {
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(conn,ps,rs);
		}
		return permissionList;
	}
	
	@Override
	public AuthenticationResponse authenticateUser(User user, String password) {
		AuthenticationResponse response = null;
		boolean valid = false;
		if(StringUtils.isNotNullorBlank(password) && user != null){
			response = new AuthenticationResponse();
			valid = authenticateUserPassword(password,user.getPassword());
			response.setResult(valid);
		}
		return response;
	}
	
	@Override
	public AuthenticationEmployeeResponse findEmployeeByEmployeeId(long employeeId) {
		AuthenticationEmployeeResponse response = null;
		Employee employee = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		boolean status = false;
		logger.info("Employee Information is being for Employee with Employee Id: "+employeeId);
		
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("select e.* from employee e where e.employeeId =?");
			ps.setLong(1,employeeId);
			rs = ps.executeQuery();
			if(rs.next()){
				response = new AuthenticationEmployeeResponse();
				employee = new Employee();
				status = true;
				
				employee.setAddress1(rs.getString("address1"));
				employee.setAddress2(rs.getString("address2"));
				employee.setCity(rs.getString("city"));
				employee.setEmailAddress(rs.getString("emailAddress"));
				employee.setFirstname(rs.getString("firstname"));
				employee.setGender(rs.getString("gender"));
				employee.setLastname(rs.getString("lastname"));
				employee.setMiddlename(rs.getString("middlename"));
				employee.setPhoneNumber(rs.getString("phoneNumber"));
				employee.setRegion(rs.getString("state"));
				employee.setCompanyId(rs.getInt("companyId"));
				employee.setEmployeeNumber(rs.getInt("employeeNumber"));
				employee.setEmployeeId(rs.getInt("employeeId"));
				employee.setEmployeeType(rs.getString("employeeType"));
				logger.info("Employee Information has being retrieved successfully for Employee with Employee Number: "+employee.getEmployeeNumber());
				
				response.setEmployee(employee);
				response.setResult(status);
				response.setMessage("Employee Information has being retrieved successfully for Employee with Employee Id: "+employee.getEmployeeId());
			}
		}catch(SQLException e){
			response.setResult(status);
			response.setEmployee(null);
			response.setMessage("Employee information could not  be retrieve.");
			logger.warn(e.getMessage(),e);		
		}catch(Exception e){
			response.setResult(status);
			response.setEmployee(null);
			response.setMessage("Employee information could not  be retrieve.");
			logger.warn(e.getMessage(),e);		
		}finally{
			closeDB(conn,ps,rs);		
		}
		return response;
		
	}
	@Override
	public AuthenticationEmployeeResponse findEmployeeByUserId(int employeeId) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AuthenticationEmployeeResponse response = null;
		Employee employee = null;
		boolean status = false;
		logger.info("Employee Information is being for Employee with Employee Id: "+employeeId);
		
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("select e.* from employee e where e.employeeId =?");
			ps.setInt(1,employeeId);
			rs = ps.executeQuery();
			if(rs.next()){
				response = new AuthenticationEmployeeResponse();
				employee = new Employee();
				status = true;
				
				employee.setAddress1(rs.getString("address1"));
				employee.setAddress2(rs.getString("address2"));
				employee.setCity(rs.getString("city"));
				employee.setEmailAddress(rs.getString("emailAddress"));
				employee.setFirstname(rs.getString("firstname"));
				employee.setGender(rs.getString("gender"));
				employee.setLastname(rs.getString("lastname"));
				employee.setMiddlename(rs.getString("middlename"));
				employee.setPhoneNumber(rs.getString("phoneNumber"));
				employee.setRegion(rs.getString("state"));
				employee.setCompanyId(rs.getInt("companyId"));
				employee.setEmployeeNumber(rs.getInt("employeeNumber"));
				employee.setEmployeeId(rs.getInt("employeeId"));
				employee.setEmployeeType(rs.getString("employeeType"));
				logger.info("Employee Information has being retrieved successfully for Employee with Employee Number: "+employee.getEmployeeNumber());
				
				response.setEmployee(employee);
				response.setResult(status);
				response.setMessage("Employee Information has being retrieved successfully for Employee with Employee Id: "+employee.getEmployeeId());
			}
		}catch(SQLException e){
			response.setResult(status);
			response.setEmployee(null);
			response.setMessage("Employee information could not  be retrieve.");
			logger.warn(e.getSQLState(),e);	
		}finally{
			closeDB(conn,ps,rs);			
		}
		return response;
	}

	@Override
	public AuthenticationEmployeeResponse findEmployeeByUsername(String username) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		AuthenticationEmployeeResponse response = null;
		Employee employee = null;

		boolean status = false;
		String companyUUID = null;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("select e.* from employee e INNER JOIN user u On e.employeeId = u.employeeId  where u.username =?");
			ps.setString(1,username);
			rs = ps.executeQuery();
			if(rs.next()){
				response = new AuthenticationEmployeeResponse();
				employee = new Employee();
				status = true;
				employee.setAddress1(rs.getString("address1"));
				employee.setAddress2(rs.getString("address2"));
				employee.setCity(rs.getString("city"));
				employee.setEmailAddress(rs.getString("emailAddress"));
				employee.setFirstname(rs.getString("firstname"));
				employee.setGender(rs.getString("gender"));
				employee.setLastname(rs.getString("lastname"));
				employee.setMiddlename(rs.getString("middlename"));
				employee.setPhoneNumber(rs.getString("phoneNumber"));
				employee.setRegion(rs.getString("region"));
				employee.setCompanyId(rs.getInt("companyId"));
				employee.setEmployeeNumber(rs.getLong("employeeNumber"));
				employee.setEmployeeId(rs.getInt("employeeId"));
				employee.setEmployeeType(rs.getString("employeeType"));
				logger.info("Employee Information has being retrieved successfully for Employee with Employee Id: "+employee.getEmployeeNumber());
				response.setCompanyId(employee.getCompanyId());
				response.setEmployee(employee);
				response.setResult(status);
				response.setCompanyUUID(companyUUID);
				response.setMessage("Employee Information has being retrieved successfully for Employee with Employee Id: "+rs.getLong("employeeNumber"));
			}
		}catch(SQLException e){
			response.setResult(status);
			response.setEmployee(null);
			response.setMessage("Employee information could not  be retrieve.");
			logger.warn(e.getMessage(),e);			
		}finally{	
			if(response == null){
				response = new AuthenticationEmployeeResponse();
				response.setResult(status);
				response.setMessage("Employee Information could not be retrieved.");
				logger.info(response.getMessage());
			}
			closeDB(conn,ps,rs);
		}
		return response;
	}
	@Override
	public AuthenticationEmployeeResponse findEmployeePermissionsByUsername(String username) {
		return null;
	}
	
	@Override
	public List<Employee> getAllEmployeeByCompanyId(int companyId) {
		List<Employee> employeeList = null;

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("select * from employee e where e.companyId =?");
			ps.setInt(1,companyId);
			rs = ps.executeQuery();
			employeeList = new ArrayList<Employee>();
			while(rs.next()){
				
				Employee employee = new Employee();
				
				employee.setAddress1(rs.getString("address1"));
				employee.setAddress2(rs.getString("address2"));
				employee.setCity(rs.getString("city"));
				employee.setEmailAddress(rs.getString("emailAddress"));
				employee.setFirstname(rs.getString("firstname"));
				employee.setGender(rs.getString("gender"));
				employee.setLastname(rs.getString("lastname"));
				employee.setMiddlename(rs.getString("middlename"));
				employee.setPhoneNumber(rs.getString("phoneNumber"));
				employee.setRegion(rs.getString("state"));
				employee.setCompanyId(rs.getInt("companyId"));
				employee.setEmployeeNumber(rs.getLong("employeeNumber"));
				employee.setEmployeeId(rs.getInt("employeeId"));
				employee.setEmployeeType(rs.getString("employeeType"));
				employeeList.add(employee);
			}
		}catch(SQLException e){
			logger.warn(e.getSQLState(),e);	
		}finally{
			closeDB(conn,ps,rs);
		}
		return employeeList;
	}
	
	private void close(ResultSet rs) throws SQLException{
		if(!rs.isClosed() && rs != null){
			rs.close();
		}
	}
	private void close(Connection conn) throws SQLException{
		if(conn != null && !conn.isClosed()){
			conn.close();
		}
	}
	private void close(PreparedStatement ps) throws SQLException{
		if(!ps.isClosed()){
			ps.close();
		}
	}
	
	private  BCryptPasswordEncoder passwdencode(){
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);
		return encoder;
	}
	
	private boolean authenticateUserPassword(String password, String hashPassword){
		boolean results = false;
		results = passwdencode().matches(password, hashPassword);
		return results;
	}


	public void saveLoginTime(String username, int companyId,Boolean status){
		Connection conn = null;
		PreparedStatement ps = null;
		
		Date date = new Date();
		String dateTime = String.valueOf(date.getTime());
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("insert into login_log(username,status,dateTime,companyId) values (?,?,?,?) ");
			ps.setString(1,username);
			ps.setString(2,status.toString());
			ps.setString(3, dateTime);
			ps.setInt(4,companyId);
			ps.executeUpdate();
		}catch(SQLException e){
			logger.warn(e.getSQLState(),e);		
		}finally{
			closeArtifacts(ps);
		}
	}
	private void closeArtifacts(PreparedStatement ps){
		try{
			close(ps);
		}catch(SQLException e){
			logger.warn(e.getSQLState(),e);			
		}	
	}
	private void closeArtifacts(PreparedStatement ps, ResultSet rs){
		try{
			close(rs);
			close(ps);
		}catch(SQLException e){
			logger.warn(e.getSQLState(),e);			
		}	
	}

	@Override
	public boolean doEmployeeEmailExist(String emailAddress) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean exist = false;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("select * from employee where emailAddress=?");
			ps.setString(1,emailAddress);
			rs = ps.executeQuery();
			if(rs.next()){
				exist = true;
			}
		}catch(SQLException e){
			exist = false;
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(conn,ps,rs);
		}
		return exist;
	}

	@Override
	public List<PermissionDTO> getAllEmployeePermission() {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		PermissionDTO permissionDTO = null;
		List<PermissionDTO> permissions = null;
		try{

			conn = dataSource.getConnection();
			ps = conn.prepareStatement("select * from permissions where permission != 'ADMINISTRATOR'");
			rs = ps.executeQuery();
			permissions = new ArrayList<PermissionDTO>();
			while(rs.next()){
				permissionDTO = new PermissionDTO();
				permissionDTO.setName(rs.getString("name"));
				permissionDTO.setPermissionId(rs.getInt("permissionId"));
				permissionDTO.setPermission(rs.getString("permission"));
				permissions.add(permissionDTO);
			}
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(conn,ps,rs);
		}
		
		return  permissions;
	}

	/**
	 * Saves Employee Information
	 */
	@Override
	public AuthenticationResponse saveEmployeeInformation(AuthenticationEmployeeRequest request) {
		logger.info("Started the process of saving Employee information for Company Id: "+request.getCompanyNumber());
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AuthenticationResponse response = null;
		EmployeeRegistrationRequest employeeResponse = null;
		String url = hostname.concat(activateEmployeeUrl);
		long key = 0;
		try{
			Long employeeNumber = AuthenticationUtils.generateEmployeeNumber();
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("insert into employee (employeeId,address1,address2,phoneNumber,city,emailAddress,employeeNumber,firstname,gender,lastname,middlename,region,companyId,positionId,employeeType) values (NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			ps.setString(1,request.getAddress1());
			ps.setString(2, request.getAddress2());
			ps.setString(3,request.getPhoneNumber());
			ps.setString(4,request.getCity());
			ps.setString(5,request.getEmailAddress());
			ps.setString(7,request.getFirstname());
			ps.setLong(6,employeeNumber);
			ps.setString(8,request.getGender());
			ps.setString(9,request.getLastname());
			ps.setString(10,request.getMiddlename());
			ps.setString(11,request.getRegion());
			ps.setLong(12,request.getCompanyId());
			ps.setLong(13,request.getPositionId());
			ps.setString(14, request.getEmployeeType());
			int Id = ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if(rs.next()){
				key = rs.getLong(Id);
				response = new AuthenticationResponse();
				response.setResult(true);
				response.setEmployeeId(key);
				response.setMessage("Employee has being saved successfully. New Employee Id:"+key);
				request.setEmployeeId(key);
				if(request.getRequestType().equalsIgnoreCase("employee")){
					employeeResponse = addTempEmployeeAccount(request,url,conn);
					String responseString =  AuthenticationUtils.convertObjectToJSON(employeeResponse);
					//employeeRegistrationTemplate.convertAndSend(responseString);		
				}
			}

			logger.info("Employee with Id: "+key+" has being saved successfully for company with Id: "+request.getCompanyNumber());
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);	
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(conn,ps,rs);
		}
		return response;
	}
	private void closeDB(Connection conn, PreparedStatement ps,ResultSet rs){
		try {
			if(conn != null){
				conn.close();
			}
			if(ps != null){
				ps.close();
			}
			if(rs != null){
				rs.close();
			}
		} catch (SQLException e) {
			logger.error(e.getMessage(),e);
		}
	}

	@Override
	public AuthenticationResponse saveLoginCredentials(AuthenticationEmployeeRequest request, AuthenticationResponse response) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		int key = 0;
		try{
			conn = dataSource.getConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement("insert into user (username,password,enabled,employeeId,companyId,lastLoggedIn,accountLocked, accountExpired) values (?,?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			ps.setString(1,request.getUsername());
			ps.setString(2,request.getHashpassword());
			ps.setBoolean(3,request.isEnabled());
			ps.setLong(4, response.getEmployeeId());
			ps.setLong(5, request.getCompanyId());
			ps.setLong(6,new Date().getTime());
			ps.setBoolean(7,request.isAccountLocked());
			ps.setBoolean(8, request.isAccountExpired());
			int Id = ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if(rs.next()){
				key = rs.getInt(Id);
			}
			int userId = rs.getInt(Id);
			response.setUserId(userId);
			if(request.getRequestType().equalsIgnoreCase("employee")){
				addUserPermissions(request.getRoleId(),request.getCompanyId(),userId,conn);
			}else{
				addCompanySecurity((int)request.getCompanyId(),conn);
			}
			conn.commit();
			response.setMessage("Employee has being saved successfully for CompanyId: "+request.getCompanyId());
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);	
			response.setResult(false);
			response.setMessage("Error occured saving Employee with Employee Id: "+request.getEmployeeId()+" and Username :"+request.getUsername());
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
			response.setResult(false);
			response.setMessage("Error occured saving Employee with Employee Id: "+request.getEmployeeId()+" and Username :"+request.getUsername());
		}finally{
			closeDB(conn,ps,rs);
			request = null;
		}
		return response;
	}
	
	@Override
	public boolean addCompanySecurity(int companyId, Connection conn){

		PreparedStatement ps;
		
		StringBuilder sbr = new StringBuilder();
		String companyUUID = AuthenticationUtils.getCompanyUUID();
		
		String uniqueKey = AuthenticationUtils.generateCompanyAccountID();
			sbr.append(uniqueKey);
			sbr.append("&");
			sbr.append(companyId);
		
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("insert into companysecurity (companySecurityId,companyId,encryptionKey,companyUUID) values (NULL,?,?,?) ");
			ps.setString(3,companyUUID);
			ps.setInt(1,companyId);
			ps.setString(2,sbr.toString());
			ps.executeUpdate();
			logger.info("Company with Id: "+companyId+" has security information saved successfully.");
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);			
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(conn,null,null);
		}
		return false;
	}


	@Override
	public AuthenticationResponse addUserPermissions(int[] roleId, long companyId, int userId, Connection conn) {
		AuthenticationResponse response = null;
		PreparedStatement ps = null;
		try{
			if(conn == null || conn.isClosed()){
				conn = dataSource.getConnection();
			}
			ps = conn.prepareStatement("insert into userpermission (userpermissionId,permissionId,userId,companyId) values (NULL,?,?,?) ");
			for(int i=0;i < roleId.length;i++){
				ps.setInt(1,roleId[i]);
				ps.setInt(2,userId);
				ps.setLong(3,companyId);	
				ps.executeUpdate();
			}
			response = new AuthenticationResponse();
			response.setResult(true);
			logger.info("Company with Id: "+companyId+" and user Id: "+userId+" has security information saved successfully.");
		}catch(SQLException e){
			response = new AuthenticationResponse();
			response.setResult(false);
			response.setMessage("Error occured adding User Permissions.");
			logger.warn(e.getMessage(),e);			
		}catch(Exception e){
			response = new AuthenticationResponse();
			response.setResult(false);
			response.setMessage("Error occured adding User Permissions.");
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(conn,ps,null);
		}
		return response;
	}
	
	public void addEmployeeLogin(int companyId,long employeeId, boolean status ){
		Session session = null;
		
		try {
			session = cassandraDB.getSession();
			Mapper<EmployeeLogin> loginMapper = new MappingManager(session).mapper(EmployeeLogin.class);
			EmployeeLogin login = new EmployeeLogin();
			login.setLoginDate(Long.toString(System.nanoTime()));
			
			loginMapper.saveAsync(login);
		} catch (PlatformException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@Override
	public AuthenticationUsernamePasswordResponse findUserByUsername(String emailAddress, String username, String lastname) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AuthenticationUsernamePasswordResponse response = null;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("select e.emailAddress,e.firstname, e.employeeId,u.companyId from employee e INNER JOIN user u  ON u.employeeId = e.employeeId where e.emailAddress =? AND e.lastname =? AND u.username = ?");
			ps.setString(1,emailAddress);
			ps.setString(2, lastname);
			ps.setString(3, username);
			rs = ps.executeQuery();
			while(rs.next()){
				response = new AuthenticationUsernamePasswordResponse();
				response.setEmailAddress(rs.getString("emailAddress"));
				response.setCompanyId(rs.getInt("companyId"));
				response.setEmployeeId(rs.getLong("employeeId"));
				response.setName(rs.getString("firstname"));
			}
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);			
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(conn,ps,rs);
		}
		
		return response;
	}


	@Override
	public AuthenticationUsernamePasswordResponse findUserByUsername(String emailAddress, String lastname) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AuthenticationUsernamePasswordResponse response = null;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("select u.username, e.emailAddress,e.firstname from employee e INNER JOIN user u  ON u.employeeId = e.employeeId where e.emailAddress =? AND e.lastname =?");
			ps.setString(1,emailAddress);
			ps.setString(2, lastname);
			rs = ps.executeQuery();
			while(rs.next()){
				response = new AuthenticationUsernamePasswordResponse();
				response.setEmailAddress(rs.getString("emailAddress"));
				response.setUsername(rs.getString("username"));
				response.setName(rs.getString("firstname"));
			}
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);			
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(conn,ps,rs);
		}
		
		return response;
	}


	@Override
	public void sendEmail(AuthenticationUsernamePasswordResponse response) {
		MessageRequest request = new MessageRequest();
		request.setEmailMessage(response.getEmailAddress());
		request.setReceiverName(response.getName());
		request.setUsername(response.getUsername());
		request.setRequestType("forgotUsername");
		logger.info("Authentication service sending request to Message Service to send email.");
		String requestString = "";
		try {
			requestString = AuthenticationUtils.convertObjectToJSON(request);
			Client client = ClientBuilder.newClient();
			Future<Boolean> result = client.target("http://localhost:8080/messageservice/rs/sendEmailforEmployee")
					.request(MediaType.APPLICATION_JSON_TYPE)
					.async()
					.post(Entity.entity(requestString, MediaType.APPLICATION_JSON),Boolean.class);
			if(result.isDone()){
				logger.info("Authentication service has completed sending request to Message Service to send email, and return message: Is Done: "+result.isDone());
			}			
		} catch (JsonProcessingException e) {
			logger.info(e.getMessage());
		}

	}


	@Override
	public AuthenticationEmployeeResponse updateEmployeeInformation(AuthenticationUpdateEmployeeRequest request) {
		Connection conn = null;
		PreparedStatement ps = null;
		AuthenticationEmployeeResponse response = null;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("update employee set address1 =?,address2 =?,phoneNumber=?,city=?,emailAddress=?,firstname=?,lastname=?,middlename=?,state=?WHERE employeeId =? and companyId =?");
			ps.setString(1,request.getAddress1());
			ps.setString(2, request.getAddress2());
			ps.setString(3,request.getPhoneNumber());
			ps.setString(4,request.getCity());
			ps.setString(5,request.getEmailAddress());
			ps.setString(6,request.getFirstname());
			ps.setString(7,request.getLastname());
			ps.setString(8,request.getMiddlename());
			ps.setString(9,request.getRegion());
			ps.setLong(10, request.getEmployeeId());
			ps.setInt(11,request.getCompanyId());
			ps.executeUpdate();
			
			response = findEmployeeByEmployeeId(request.getEmployeeId());
			response.setMessage("Employee has being saved successfully.");
			logger.info("Employee with Id: "+request.getEmployeeId()+" has being updated successfully for company with Id: "+request.getCompanyId());
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);	
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(conn,ps,null);
		}
		return response;
	}

	/**
	 * This method of lock the user account, add temp login 
	 * data for the user to change his/her password.
	 */
	private void addTempPasswordforChangePassword(AuthenticationUsernamePasswordResponse response){
		Connection conn  = null;
		PreparedStatement ps = null;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("insert into tempUserAccount (username,token,dateCreated,companyId,tempPassword) value (?,?,?,?,?);");
			response.setVerificationCode(AuthenticationUtils.generatePasswordUniqueKey());
			response.setTempPassword(AuthenticationUtils.generateTempPassword());
			ps.setString(1, response.getUsername());
			ps.setString(2,response.getVerificationCode() );
			ps.setLong(3, new Date().getTime());
			ps.setInt(4, response.getCompanyId());
			ps.setString(5, response.getTempPassword());
			ps.executeUpdate();
			ps = conn.prepareStatement("update user u set u.accountLocked=? where u.username=?");
			ps.setBoolean(1, true);
			ps.setString(2, response.getUsername());
			ps.executeUpdate();
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);	
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(conn,ps,null);
		}
	}
	
	@Override
	public AuthenticationUsernamePasswordResponse forgotPasswordService(String emailAddress, String username,String lastname,String changePasswordUrl) {
		AuthenticationUsernamePasswordResponse response = findUserByUsername(emailAddress,username,lastname);
		String convertedResponse = null;
		try{
			if(response != null){
				response.setUsername(username);
				addTempPasswordforChangePassword(response);
				response.setResult(true);
				response.setUrl(changePasswordUrl);
				convertedResponse = AuthenticationUtils.convertObjectToJSON(response);
				changePasswordTemplate.convertAndSend(convertedResponse);
			}else{
				response = new AuthenticationUsernamePasswordResponse();
				response.setResult(false);
			}			
		}catch(IOException e){
			logger.warn(e.getMessage(),e);	
		}finally{
			convertedResponse = null;
		}

		return response;
	}


	@Override
	public boolean validatePasswordToken(String token, LocalDateTime dateTime) {
		Connection conn  = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean result = false;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("select * from tempUserAccount t  where t.token =?");
			ps.setString(1, token);
			rs = ps.executeQuery();
			TempUserAccount userAccount = null;
			while(rs.next()){
				userAccount = new TempUserAccount();
				userAccount.setCompanyId(rs.getInt("companyId"));
				userAccount.setDateCreated(rs.getLong("dateCreated"));
				userAccount.setTempPassword(rs.getString("tempPassword"));
				userAccount.setToken(rs.getString("token"));
				userAccount.setUsername(rs.getString("username"));
			}
			if(userAccount != null){
				boolean tempValid  = false;
				tempValid = isTokenNotExpired(dateTime,userAccount.getDateCreated());

				result = tempValid;
			}
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);	
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(conn,ps,rs);
		}
		return result;
	}
	
	private boolean isTokenNotExpired(LocalDateTime dateTime,long storedDate){
		Date date1 = new Date(storedDate);
		boolean isValid = false;
		LocalDateTime dateTime1 = LocalDateTime.ofInstant(date1.toInstant(), ZoneId.systemDefault());

		isValid = ((dateTime1.plusMinutes(60).isAfter(dateTime)));
		return isValid;
		
	}


	@Override
	public AuthenticationChangePasswordResponse forgotPassword(AuthenticationChangePasswordRequest request) {
		AuthenticationChangePasswordResponse response = null;
		String confirmationCode = request.getConfirmationCode();
		
		String newpasswd = request.getNewPassword();
		String token = request.getPasswordToken();
		TempUserAccount userAccount = null;
		userAccount = getTempAccount(confirmationCode,token,newpasswd);
		if(userAccount != null){
			response = new AuthenticationChangePasswordResponse();
			response.setResult(true);
			response.setRequestType(request.getRequestType());
		}
		return response;
	}


	private TempUserAccount getTempAccount(String confirmationCode, String token,String newPasswd) {
		Connection conn  = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		TempUserAccount userAccount = null;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("select * from tempUserAccount t  where t.token =? and t.tempPassword =?");
			ps.setString(1, token);
			ps.setString(2, confirmationCode);
			rs = ps.executeQuery();
			
			while(rs.next()){
				userAccount = new TempUserAccount();
				userAccount.setTempId(rs.getInt("tempId"));
				userAccount.setCompanyId(rs.getInt("companyId"));
				userAccount.setDateCreated(rs.getLong("dateCreated"));
				userAccount.setTempPassword(rs.getString("tempPassword"));
				userAccount.setToken(rs.getString("token"));
				userAccount.setUsername(rs.getString("username"));
			}
			if(userAccount != null){
				ps.clearParameters();
				String hashPasswd = passwdencode().encode(newPasswd);
				ps = conn.prepareStatement("update user u set u.password =? ,u.accountLocked =? where u.username =?");
				ps.setString(1,hashPasswd );
				ps.setBoolean(2,false);
				ps.setString(3, userAccount.getUsername());
				ps.executeUpdate();
				ps.clearParameters();
				ps = conn.prepareStatement("delete from tempUserAccount where tempId =?");
				ps.setInt(1, userAccount.getTempId());
				ps.executeUpdate();				
			}
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);	
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(conn,ps,rs);
		}
		return userAccount;
	}


	@Override
	public AuthenticationChangePasswordResponse changePassword(AuthenticationChangePasswordRequest request) {
		AuthenticationChangePasswordResponse response = null;
		String newpasswd = request.getNewPassword();
		String oldpasswd = request.getOldPassword();
		String username = request.getUsername();
		User userAccount = null;
		Connection conn  = null;
		boolean match = false;
		try{
			conn = dataSource.getConnection();
			
			userAccount = getUserAccount(oldpasswd,newpasswd,username,conn);
			if(userAccount != null){
				match = authenticateUserPassword(oldpasswd,userAccount.getPassword());
				if(match){		
					updateUserAccount(conn,username,newpasswd);
					response = new AuthenticationChangePasswordResponse();
					response.setResult(match);
					response.setMessage("Password Change was Successful. Login back using your new credentials");
					response.setRequestType(request.getRequestType());	
				}else{
					response = new AuthenticationChangePasswordResponse();
					response.setResult(match);
					response.setRequestType(request.getRequestType());		
					response.setMessage("The information you provided are incorrect. Try again using the correct login credentials");
				}
		}else{
			response = new AuthenticationChangePasswordResponse();
			response.setResult(match);
			response.setRequestType(request.getRequestType());		
			response.setMessage("The information you provided are incorrect. Try again using the correct login credentials");
		}
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
		}finally{
				closeDB(conn,null,null);
		}
		return response;
	}


	private void updateUserAccount(Connection conn, String username, String newpasswd) {
		PreparedStatement ps = null;
		try{
			ps = conn.prepareStatement("update user u set u.password =?, u.accountLocked=? where u.username =?");
			String hashPassword = passwdencode().encode(newpasswd);
			ps.setString(1,hashPassword);
			ps.setBoolean(2,false);
			ps.setString(3,username);
			ps.executeUpdate();
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);	
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
		}finally{
			closeArtifacts(ps);
		}
	}


	private User getUserAccount(String oldpasswd, String newpasswd,String username,Connection conn) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = null;
		try{
			ps = conn.prepareStatement("select * from user where username =?");
			ps.setString(1,username);
			rs = ps.executeQuery();
			if(rs.next()){
				user = new User();
				user.setCompanyId(rs.getInt("companyId"));
				user.setPassword(rs.getString("password"));
				user.setEnabled(rs.getBoolean("enabled"));
				user.setUsername(rs.getString("username"));
				user.setUserId(rs.getInt("userId"));
				user.setAccountExpired(rs.getBoolean("accountExpired"));
				user.setAccountLocked(rs.getBoolean("accountLocked"));
			}
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);	
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
		}finally{
			closeArtifacts(ps,rs);
		}
		return user;
	}

	private EmployeeRegistrationRequest addTempEmployeeAccount(AuthenticationEmployeeRequest request,String activateEmployeeUrl,Connection conn){
		EmployeeRegistrationRequest employeeRegistrationRequest  = new EmployeeRegistrationRequest();
		employeeRegistrationRequest.setUrl(activateEmployeeUrl);
		employeeRegistrationRequest.setEmailAddress(request.getEmailAddress());
		employeeRegistrationRequest.setName(request.getFirstname()+" "+request.getLastname());
		employeeRegistrationRequest.setTempPassword(AuthenticationUtils.generateTempPassword());
		employeeRegistrationRequest.setEmployeeId(request.getEmployeeId());
		employeeRegistrationRequest.setVerificationCode(AuthenticationUtils.generatePasswordUniqueKey());

		PreparedStatement ps = null;
		try{
			int companyId = Integer.parseInt(Long.toString(request.getCompanyId()));
			employeeRegistrationRequest.setCompanyId(companyId);
			if (conn == null || conn.isClosed()){
				conn = dataSource.getConnection();
			}			
			ps = conn.prepareStatement("insert into tempEmployeeAccount (employeeId,emailAddress,verificationCode,companyId,dateCreated,tempPassword,roleId) values (?,?,?,?,?,?,?);");
			ps.setLong(1,employeeRegistrationRequest.getEmployeeId());
			ps.setString(2,employeeRegistrationRequest.getEmailAddress());
			ps.setString(3,employeeRegistrationRequest.getVerificationCode());
			ps.setInt(4,employeeRegistrationRequest.getCompanyId());
			ps.setLong(5,new Date().getTime());
			ps.setString(6,employeeRegistrationRequest.getTempPassword());
			ps.setInt(7, request.getRoleId()[0]);
			ps.executeUpdate();
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);	
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(conn,ps,null);
		}
		return employeeRegistrationRequest;
	}
	@Override
	public boolean doPhoneNumberExist(String phoneNumber) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean exist = false;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("select * from employee where phoneNumber=?");
			ps.setString(1,phoneNumber);
			rs = ps.executeQuery();
			if(rs.next()){
				exist = true;
			}
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);	
			exist = false;
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
			exist = false;
		}finally{
			closeDB(conn,ps,rs);
		}
		return exist;
	}


	@Override
	public boolean validateEmployeeToken(String token, LocalDateTime dateTime) {
		Connection conn  = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean result = false;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("select * from tempEmployeeAccount t  where t.verificationCode =?");
			ps.setString(1, token);
			rs = ps.executeQuery();
			TempEmployeeAccount userAccount = null;
			while(rs.next()){
				userAccount = new TempEmployeeAccount();
				userAccount.setCompanyId(rs.getInt("companyId"));
				userAccount.setDateCreated(rs.getLong("dateCreated"));
				userAccount.setTempPassword(rs.getString("tempPassword"));
				userAccount.setToken(rs.getString("verificationCode"));
				userAccount.setEmailAddress(rs.getString("emailAddress"));
			}
			if(userAccount != null){
				boolean tempValid  = false;
				tempValid = isTokenNotExpired(dateTime,userAccount.getDateCreated());

				result = tempValid;
			}
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);	
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
		}finally{
			rs = null;
			ps = null;
		}
		return result;
	}


	@Override
	public AuthenticationChangePasswordResponse activateEmployee(AuthenticationActivateEmployee request) {
		AuthenticationChangePasswordResponse response = null;
		String username = request.getUsername();
		String password = request.getHashPassword();
		String tempPassword = request.getTempPassword();
		
		Connection conn  = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		int key = 0;
		TempEmployeeAccount employeeAccount = null;
		try{
			conn = dataSource.getConnection();
			employeeAccount = validateEmployeeTempPassword(conn,tempPassword,request.getToken());
			if(employeeAccount != null){
				ps = conn.prepareStatement("insert into user (username,password,enabled,employeeId,companyId,lastLoggedIn,accountLocked, accountExpired) values (?,?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
				ps.setString(1,username);
				ps.setString(2,password);
				ps.setBoolean(3,request.isAccountEnbaled());
				ps.setInt(4, employeeAccount.getEmployeeId());
				ps.setInt(5, employeeAccount.getCompanyId());
				ps.setLong(6,new Date().getTime());
				ps.setBoolean(7,request.isAccountLocked());
				ps.setBoolean(8, request.isAccountExpired());
				int Id = ps.executeUpdate();
				rs = ps.getGeneratedKeys();
				if(rs.next()){
					key = rs.getInt(Id);
				}
				int userId = key;
				addUserPermissions(employeeAccount.getRoleId(),employeeAccount.getCompanyId(),userId,conn);
				if(request.getRequestType().equalsIgnoreCase("employee")){
					updateTempEmployeeAccount(employeeAccount,conn);
				}
				response = new AuthenticationChangePasswordResponse();
				response.setResult(true);
				response.setMessage("Employee has being updaed successfully.");
			}else{
				response = new AuthenticationChangePasswordResponse();
				response.setResult(false);
				response.setMessage("Employee information has expired or currentlt not available.");
			}
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);	
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(conn,ps,null);
		}
		return response;
	}


	private void updateTempEmployeeAccount(TempEmployeeAccount employeeAccount, Connection conn) {
		PreparedStatement ps = null;
		try{
			
			ps = conn.prepareStatement("delete from tempEmployeeAccount where tempId =?");
			ps.setInt(1, employeeAccount.getTempId());
			ps.executeUpdate();
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);	
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(null,ps,null);
		}
	}


	private TempEmployeeAccount validateEmployeeTempPassword(Connection conn, String tempPassword, String token) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		TempEmployeeAccount employeeAccount = null;
		
		try{
			ps = conn.prepareStatement("select * from tempEmployeeAccount where tempPassword =? and verificationCode =?");
			ps.setString(1, tempPassword);
			ps.setString(2,token);
			rs = ps.executeQuery();
			if(rs.next()){
				employeeAccount = new TempEmployeeAccount();
				employeeAccount.setTempId(rs.getInt("tempId"));
				employeeAccount.setCompanyId(rs.getInt("companyId"));
				employeeAccount.setEmailAddress(rs.getString("emailAddress"));
				employeeAccount.setEmployeeId(rs.getInt("employeeId"));
				employeeAccount.setRoleId(rs.getInt("employeeRole"));
			}
		}catch(SQLException e){
			logger.warn(e.getMessage(),e);	
		}catch(Exception e){
			logger.warn(e.getMessage(),e);	
		}closeDB(null,ps,null);

		return employeeAccount;
	}


	@Override
	public AuthenticationResponse addUserPermissions(int[] roleId, long companyId, int userId) {
		AuthenticationResponse response = addUserPermissions(roleId,companyId,userId,null);
		return response;
	}

	private Integer findUserByEmployeeId(int employeeId,Connection conn){
		Integer userId = 0;
		try{
			if(conn.isClosed()){
				conn = dataSource.getConnection();
			}
			PreparedStatement ps = null;
			ResultSet rs = null;		
			ps = conn.prepareStatement("select u.* from user u where u.employeeId =?");
			ps.setInt(1,employeeId);
			rs = ps.executeQuery();
			if(rs.next()){
				userId = rs.getInt("userId");
			}
		}catch(SQLException e){
			logger.warn(e.getMessage(), e);
		}catch(Exception e){
			logger.warn(e.getMessage(), e);
		}
		return userId;
	}
	
	@Override
	public AuthenticationEmployeeResponse findEmployeeByEmployeeId(int employeeId, int companyId) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		AuthenticationEmployeeResponse response = null;
		Employee employee = null;
		boolean status = false;
		logger.info("Employee Information is being for Employee with Employee Id: "+employeeId);
		
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("select e.* from employee e where e.employeeId =? and e.companyId =?");
			ps.setInt(1,employeeId);
			ps.setInt(2,companyId);
			rs = ps.executeQuery();
			if(rs.next()){
				int userId = findUserByEmployeeId(employeeId,conn);
				List<String> permission = findEmployeePermissionByUserId(conn, userId, companyId);
				response = new AuthenticationEmployeeResponse();
				employee = new Employee();
				status = true;
				
				employee.setAddress1(rs.getString("address1"));
				employee.setAddress2(rs.getString("address2"));
				employee.setCity(rs.getString("city"));
				employee.setEmailAddress(rs.getString("emailAddress"));
				employee.setFirstname(rs.getString("firstname"));
				employee.setGender(rs.getString("gender"));
				employee.setLastname(rs.getString("lastname"));
				employee.setMiddlename(rs.getString("middlename"));
				employee.setPhoneNumber(rs.getString("phoneNumber"));
				employee.setRegion(rs.getString("state"));
				employee.setCompanyId(rs.getInt("companyId"));
				employee.setEmployeeNumber(rs.getInt("employeeNumber"));
				employee.setEmployeeId(rs.getInt("employeeId"));
				logger.info("Employee Information has being retrieved successfully for Employee with Employee Number: "+employee.getEmployeeNumber());
				if(permission.size() > 0 && permission.get(0) != null){
					response.setPermissions(permission.get(0));
				}else{
					response.setPermissions("Employee has not being assigned a role yet.");
				}
				
				response.setEmployee(employee);
				response.setResult(status);
				response.setMessage("Employee Information has being retrieved successfully for Employee with Employee Id: "+employee.getEmployeeId());
			}
		}catch(SQLException e){
			response = new AuthenticationEmployeeResponse();
			response.setResult(status);
			response.setEmployee(null);
			response.setMessage("Employee information could not  be retrieve.");
			logger.warn(e.getSQLState(),e);	
		}catch(Exception e){
			response = new AuthenticationEmployeeResponse();
			response.setResult(status);
			response.setEmployee(null);
			response.setMessage("Employee information could not  be retrieve.");
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(conn,ps,rs);
		}
		return response;
	}


	@Override
	public AuthenticationResponse deleteCompanyEmployeesAndUsers(Integer companyId) {
		Connection conn = null;
		PreparedStatement ps = null;
		AuthenticationResponse response = null;
		boolean status = false;
		try{
			conn = dataSource.getConnection();
			ps = conn.prepareStatement("delete from employee where companyId =?");
			ps.setInt(1,companyId);
			ps.executeUpdate();
			logger.info("All Employee's for Company with Id: "+companyId+" have being deleted");
			ps = conn.prepareStatement("delete from user where companyId =?");
			ps.setInt(1,companyId);
			ps.executeUpdate();
			ps = conn.prepareStatement("delete from userpermission where companyId =?");
			ps.setInt(1,companyId);
			ps.executeUpdate();			
			logger.info("All Employee's for Company with Id: "+companyId+" have being deleted");
			response = new AuthenticationResponse();
			response.setResult(true);
			response.setMessage("All Company Employee's and their login credentials have being deleted for Company Id: "+companyId);
		}catch(SQLException e){
			response = new AuthenticationResponse();
			response.setResult(status);
			response.setEmployee(null);
			response.setMessage("Employee information could not  be retrieve.");
			logger.warn(e.getSQLState(),e);	
		}catch(Exception e){
			response = new AuthenticationResponse();
			response.setResult(status);
			response.setEmployee(null);
			response.setMessage("Employee information could not  be retrieve.");
			logger.warn(e.getMessage(),e);	
		}finally{
			closeDB(conn,ps,null);
		}
		return response;
	}


	@Override
	public AuthenticationResponse addUserPermissions(int roleId, long companyId, int userId) {
		int[] role = {roleId};
		AuthenticationResponse response = addUserPermissions(role,companyId,userId,null);
		return response;
	}


	@Override
	public AuthenticationResponse saveLoginInformation(AuthenicationUserRequest request) {
		// TODO Auto-generated method stub
		return null;
	}


}
