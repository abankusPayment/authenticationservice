package com.proteck.authentication.utils;

import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;

public class StringUtils {

	private static final String BLANK_SPACE = " ";
	
	public static boolean isNotNullorBlank(String s){
		
		if(s.trim() != null || !s.trim().equals(null) || s.trim().length() > 0 || s.trim() != StringUtils.BLANK_SPACE){
			return true;
		}
		return false;
	}
    private static String getBasicAuthHeader(String userId, String password) {
        return "Basic " + base64Encode(userId + ":" + password);
    }

    public static String base64Encode(String token) {
        byte[] encodedBytes = Base64.encodeBase64(token.getBytes());
        return new String(encodedBytes, Charset.forName("UTF-8"));
    }
}
