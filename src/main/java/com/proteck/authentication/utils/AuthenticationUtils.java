package com.proteck.authentication.utils;

import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.proteck.authentication.exception.PlatformException;

public class AuthenticationUtils {
	private static final String SECURITY_ALGORITHM = "AES/CBC/PKCS5PADDING";

	private static final String SECURITY_KEY = "Abankwah11kOFI5514*Macintosh-1250SmithfieldRoadAptF7NorthProvidenceIAmAChildOfGod";
	
	private static final String SECURITY_ITERATIONS = "65536";
	
	private static final String ENCODING = "UTF-8";
	
	private static final String AES_ALGORITHM = "AES";
	
	private static final SecureRandom random = new SecureRandom();

	/*** Encoding Type */
	private static final String ENCODING_TYPE = "AES/CBC/PKCS5Padding";	
	
	public static <T>  T convertFromJSON(Class<T> cls, String json) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		T type = null;
		if(json != null){
			type = mapper.readValue(json,cls);
		}
		
		return type;
	}
	
	public static String[] convertListToArray(List<String> list){
		if(list == null){
			return null;
		}
		return list.toArray(new String[list.size()]);
	}
	
	public static List<String> convertArraysToList(String[] arr){
		List<String> list = null;
		if(arr != null && arr.length > 0){
			list = Arrays.asList(arr);
		}
		return list;
	}
	
	public static long generateEmployeeNumber(){		
		StringBuilder sbr = new StringBuilder();
		String rand = RandomStringUtils.randomNumeric(8);
		sbr.append(rand);
		
		Long empNumber = Long.valueOf(sbr.toString());
		return empNumber;
	}
	
	/**
	 * Use for password change and forgot password verification
	 * @return password verification code
	 */
    public static String generatePasswordUniqueKey(){
    	int length = 32;
    	String alphabet = 
    	        new String("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!"); 
    	int n = alphabet.length();

    	String result = new String(); 
    	for (int i=0; i<length; i++) {
    	    result = result + alphabet.charAt(random.nextInt(n));
    	}
    	return result;
    }
    
    public static String getCompanyUUID(){
    	StringBuilder builder = new StringBuilder();
    	String rand = RandomStringUtils.random(6);
    	String auth_token = UUID.randomUUID().toString();
    	builder.append(auth_token.replace("-", ""));
    	builder.append(rand);
    	
    	return builder.toString();
    }
    public static String decryptUsername(String username) throws PlatformException{
    	
    	try {
			Cipher cipher = Cipher.getInstance(ENCODING_TYPE);
			byte[] keyBytes = new byte[16];
			byte[] b = AuthenticationUtils.SECURITY_KEY.getBytes(AuthenticationUtils.ENCODING);
			int len = b.length;
			if (len > keyBytes.length) {
				len = keyBytes.length;
			}
			System.arraycopy(b, 0, keyBytes, 0, len);
			SecretKeySpec keySpec = new SecretKeySpec(keyBytes, AuthenticationUtils.AES_ALGORITHM);
			IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
			cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
			byte[] results = cipher.doFinal(new Base64().decode(username.getBytes()));
			return new String(results, AuthenticationUtils.ENCODING);

		} catch (Exception e) {
			PlatformException ace = new PlatformException(e);
			throw ace;
		}
    }
    
    /**
     * Company secret token
     * @return
     */
    public static String generateCompanyAccountID() {
        return new BigInteger(130, random).toString(32);
      }
    
    public static String decryptPassword(String password) throws PlatformException{
    	
    	try {
			Cipher cipher = Cipher.getInstance(ENCODING_TYPE);
			byte[] keyBytes = new byte[16];
			byte[] b = AuthenticationUtils.SECURITY_KEY.getBytes(AuthenticationUtils.ENCODING);
			int len = b.length;
			if (len > keyBytes.length) {
				len = keyBytes.length;
			}
			System.arraycopy(b, 0, keyBytes, 0, len);
			SecretKeySpec keySpec = new SecretKeySpec(keyBytes, AuthenticationUtils.AES_ALGORITHM);
			IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
			cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
			byte[] results = cipher.doFinal(new Base64().decode(password.getBytes()));
			return new String(results, AuthenticationUtils.ENCODING);

		} catch (Exception e) {
			PlatformException ace = new PlatformException(e);
			throw ace;
		}
    }
    
    public static String convertObjectToJSON(Object object) throws JsonProcessingException{
    	String jsonString = null;
    	if(object instanceof Object){
    		ObjectMapper mapper = new ObjectMapper();
    		jsonString = mapper.writeValueAsString(object);
    	}
    	
    	return jsonString;
    }

	public static String generateTempPassword() {
		
		return RandomStringUtils.randomAlphanumeric(10);
	}

	private static BCryptPasswordEncoder passwdencode(){
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);
		return encoder;
	}
	
	public static String encryptPassword(String password) {
		return passwdencode().encode(password);
	}
}
