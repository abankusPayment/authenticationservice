package com.proteck.authentication.database.configuration;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DBConfiguration {

	private static final Logger logger = LoggerFactory.getLogger(DBConfiguration.class.getName());
	
	private static final DBConfiguration db = new DBConfiguration();
	
	private @Value("${authUrl}") String databaseUrl;
	
	private @Value("${authUsername}") String username = "root";
	
	private @Value("${authPassword}") String password = "macintosh1";
	
	private String driverName = "com.mysql.jdbc.Driver";
	
	
	public DBConfiguration() {
		super();
	}


	public static DBConfiguration getInstance(){
		return db;
	}
	public Connection getConnection(){
		PoolProperties pool = new PoolProperties();
		pool.setUrl(databaseUrl);
		pool.setUsername(username);
		pool.setDriverClassName(driverName);
		pool.setPassword(password);
        pool.setJmxEnabled(true);
        pool.setTestWhileIdle(false);
        pool.setTestOnBorrow(true);
        pool.setValidationQuery("SELECT 1");
        pool.setTestOnReturn(false);
        pool.setValidationInterval(30000);
        pool.setTimeBetweenEvictionRunsMillis(30000);
        pool.setMaxActive(10);
        pool.setInitialSize(5);
        pool.setFairQueue(true);
        pool.setMaxWait(100000);
        pool.setRemoveAbandonedTimeout(60);
        pool.setMinEvictableIdleTimeMillis(30000);
        pool.setMinIdle(3);
        pool.setLogAbandoned(false);
        pool.setRemoveAbandoned(true);
        pool.setJdbcInterceptors(
          "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"+
          "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
		Connection connection = null;
		DataSource ds = new DataSource();
		ds.setUsername(username);
		ds.setPassword(password);
		ds.setUrl(databaseUrl);
		ds.setDriverClassName(driverName);
		ds.setPoolProperties(pool);
        try {
        	connection = ds.getConnection();
        	
			
		} catch (SQLException e) {
			logger.warn(e.getSQLState());
			logger.warn(e.getMessage(),e);
		}
		return connection;
	}

}
