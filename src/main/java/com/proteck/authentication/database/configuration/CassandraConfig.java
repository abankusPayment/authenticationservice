package com.proteck.authentication.database.configuration;

import java.util.concurrent.Semaphore;

import org.apache.log4j.Logger;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.proteck.authentication.exception.PlatformException;

public class CassandraConfig {
	
	private static final Logger logger = Logger.getLogger(CassandraConfig.class.getName());
	
	private static final String HOSTNAME = "127.0.0.1";

	private Cluster cluster;
	
	private Session session;
	
	
	private Semaphore semaphore = new Semaphore(10,  true);
	
	
	public Session getSession() throws PlatformException{
		
		try{
			semaphore.acquire();
			cluster = Cluster.builder().addContactPoint(HOSTNAME).build();
			session = cluster.connect();
			return session;
		}catch(InterruptedException e){
			PlatformException ace = new PlatformException(e);
			logger.warn(e.getMessage(),e);
			throw ace;
		}finally{
			close();
			semaphore.release();
		}
	}
	
	public void close() {
		   session.close();
		   cluster.close();
		}

}
